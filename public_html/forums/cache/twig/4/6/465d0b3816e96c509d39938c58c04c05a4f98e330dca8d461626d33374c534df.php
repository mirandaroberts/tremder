<?php

/* captcha_gd_acp.html */
class __TwigTemplate_465d0b3816e96c509d39938c58c04c05a4f98e330dca8d461626d33374c534df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "captcha_gd_acp.html", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "
<a id=\"maincontent\"></a>
\t<a href=\"";
        // line 4
        echo (isset($context["U_ACTION"]) ? $context["U_ACTION"] : null);
        echo "\" style=\"float: ";
        echo (isset($context["S_CONTENT_FLOW_END"]) ? $context["S_CONTENT_FLOW_END"] : null);
        echo ";\">&laquo; ";
        echo $this->env->getExtension('phpbb')->lang("BACK");
        echo "</a>

<h1>";
        // line 6
        echo $this->env->getExtension('phpbb')->lang("ACP_VC_SETTINGS");
        echo "</h1>

<p>";
        // line 8
        echo $this->env->getExtension('phpbb')->lang("ACP_VC_SETTINGS_EXPLAIN");
        echo "</p>


<form id=\"acp_captcha\" method=\"post\" action=\"";
        // line 11
        echo (isset($context["U_ACTION"]) ? $context["U_ACTION"] : null);
        echo "\">

<fieldset>
<legend>";
        // line 14
        echo $this->env->getExtension('phpbb')->lang("GENERAL_OPTIONS");
        echo "</legend>

<dl>
\t<dt><label for=\"captcha_gd_foreground_noise\">";
        // line 17
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_FOREGROUND_NOISE");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_FOREGROUND_NOISE_EXPLAIN");
        echo "</span></dt>
\t<dd><label><input id=\"captcha_gd_foreground_noise\" name=\"captcha_gd_foreground_noise\" value=\"1\" class=\"radio\" type=\"radio\"";
        // line 18
        if ((isset($context["CAPTCHA_GD_FOREGROUND_NOISE"]) ? $context["CAPTCHA_GD_FOREGROUND_NOISE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("YES");
        echo "</label>
\t\t<label><input name=\"captcha_gd_foreground_noise\" value=\"0\" class=\"radio\" type=\"radio\"";
        // line 19
        if ( !(isset($context["CAPTCHA_GD_FOREGROUND_NOISE"]) ? $context["CAPTCHA_GD_FOREGROUND_NOISE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("NO");
        echo "</label></dd>
</dl>
<dl>
\t<dt><label for=\"captcha_gd_x_grid\">";
        // line 22
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_X_GRID");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_X_GRID_EXPLAIN");
        echo "</span></dt>
\t<dd><input id=\"captcha_gd_x_grid\" name=\"captcha_gd_x_grid\" value=\"";
        // line 23
        echo (isset($context["CAPTCHA_GD_X_GRID"]) ? $context["CAPTCHA_GD_X_GRID"] : null);
        echo "\" type=\"number\" /></dd>
</dl>
<dl>
\t<dt><label for=\"captcha_gd_y_grid\">";
        // line 26
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_Y_GRID");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_Y_GRID_EXPLAIN");
        echo "</span></dt>
\t<dd><input id=\"captcha_gd_y_grid\" name=\"captcha_gd_y_grid\" value=\"";
        // line 27
        echo (isset($context["CAPTCHA_GD_Y_GRID"]) ? $context["CAPTCHA_GD_Y_GRID"] : null);
        echo "\" type=\"number\" /></dd>
</dl>
<dl>
\t<dt><label for=\"captcha_gd_wave\">";
        // line 30
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_WAVE");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_WAVE_EXPLAIN");
        echo "</span></dt>
\t<dd><label><input id=\"captcha_gd_wave\" name=\"captcha_gd_wave\" value=\"1\" class=\"radio\" type=\"radio\"";
        // line 31
        if ((isset($context["CAPTCHA_GD_WAVE"]) ? $context["CAPTCHA_GD_WAVE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("YES");
        echo "</label>
\t\t<label><input name=\"captcha_gd_wave\" value=\"0\" class=\"radio\" type=\"radio\"";
        // line 32
        if ( !(isset($context["CAPTCHA_GD_WAVE"]) ? $context["CAPTCHA_GD_WAVE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("NO");
        echo "</label>
</dd>
</dl>
<dl>
\t<dt><label for=\"captcha_gd_3d_noise\">";
        // line 36
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_3D_NOISE");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_3D_NOISE_EXPLAIN");
        echo "</span></dt>
\t<dd><label><input id=\"captcha_gd_3d_noise\" name=\"captcha_gd_3d_noise\" value=\"1\" class=\"radio\" type=\"radio\"";
        // line 37
        if ((isset($context["CAPTCHA_GD_3D_NOISE"]) ? $context["CAPTCHA_GD_3D_NOISE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("YES");
        echo "</label>
\t\t<label><input name=\"captcha_gd_3d_noise\" value=\"0\" class=\"radio\" type=\"radio\"";
        // line 38
        if ( !(isset($context["CAPTCHA_GD_3D_NOISE"]) ? $context["CAPTCHA_GD_3D_NOISE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("NO");
        echo "</label>
</dd>
</dl>
<dl>
\t<dt><label for=\"captcha_gd_fonts\">";
        // line 42
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_FONTS");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_GD_FONTS_EXPLAIN");
        echo "</span></dt>
\t<dd><label><input id=\"captcha_gd_fonts\" name=\"captcha_gd_fonts\" value=\"1\" class=\"radio\" type=\"radio\"";
        // line 43
        if (((isset($context["CAPTCHA_GD_FONTS"]) ? $context["CAPTCHA_GD_FONTS"] : null) == 1)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_FONT_DEFAULT");
        echo "</label>
\t\t<label><input name=\"captcha_gd_fonts\" value=\"2\" class=\"radio\" type=\"radio\"";
        // line 44
        if (((isset($context["CAPTCHA_GD_FONTS"]) ? $context["CAPTCHA_GD_FONTS"] : null) == 2)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_FONT_NEW");
        echo "</label>
\t\t<label><input name=\"captcha_gd_fonts\" value=\"3\" class=\"radio\" type=\"radio\"";
        // line 45
        if (((isset($context["CAPTCHA_GD_FONTS"]) ? $context["CAPTCHA_GD_FONTS"] : null) == 3)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb')->lang("CAPTCHA_FONT_LOWER");
        echo "</label>

</dd>
</dl>

</fieldset>
<fieldset>
\t<legend>";
        // line 52
        echo $this->env->getExtension('phpbb')->lang("PREVIEW");
        echo "</legend>
\t";
        // line 53
        if ((isset($context["CAPTCHA_PREVIEW"]) ? $context["CAPTCHA_PREVIEW"] : null)) {
            // line 54
            echo "\t";
            $location = (("" . (isset($context["CAPTCHA_PREVIEW"]) ? $context["CAPTCHA_PREVIEW"] : null)) . "");
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate((("" . (isset($context["CAPTCHA_PREVIEW"]) ? $context["CAPTCHA_PREVIEW"] : null)) . ""), "captcha_gd_acp.html", 54)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
            // line 55
            echo "\t";
        }
        // line 56
        echo "
</fieldset>

<fieldset>
\t<legend>";
        // line 60
        echo $this->env->getExtension('phpbb')->lang("ACP_SUBMIT_CHANGES");
        echo "</legend>
\t<p class=\"submit-buttons\">
\t\t<input class=\"button1\" type=\"submit\" id=\"submit\" name=\"submit\" value=\"";
        // line 62
        echo $this->env->getExtension('phpbb')->lang("SUBMIT");
        echo "\" />&nbsp;
\t\t<input class=\"button2\" type=\"reset\" id=\"reset\" name=\"reset\" value=\"";
        // line 63
        echo $this->env->getExtension('phpbb')->lang("RESET");
        echo "\" />&nbsp;
\t\t<input class=\"button2\" type=\"submit\" id=\"preview\" name=\"preview\" value=\"";
        // line 64
        echo $this->env->getExtension('phpbb')->lang("PREVIEW");
        echo "\" />&nbsp;
\t</p>

\t<input type=\"hidden\" name=\"select_captcha\" value=\"";
        // line 67
        echo (isset($context["CAPTCHA_NAME"]) ? $context["CAPTCHA_NAME"] : null);
        echo "\" />
\t<input type=\"hidden\" name=\"configure\" value=\"1\" />

\t";
        // line 70
        echo (isset($context["S_FORM_TOKEN"]) ? $context["S_FORM_TOKEN"] : null);
        echo "
</fieldset>
</form>

";
        // line 74
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "captcha_gd_acp.html", 74)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
    }

    public function getTemplateName()
    {
        return "captcha_gd_acp.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  267 => 74,  260 => 70,  254 => 67,  248 => 64,  244 => 63,  240 => 62,  235 => 60,  229 => 56,  226 => 55,  213 => 54,  211 => 53,  207 => 52,  193 => 45,  185 => 44,  177 => 43,  170 => 42,  159 => 38,  151 => 37,  144 => 36,  133 => 32,  125 => 31,  118 => 30,  112 => 27,  105 => 26,  99 => 23,  92 => 22,  82 => 19,  74 => 18,  67 => 17,  61 => 14,  55 => 11,  49 => 8,  44 => 6,  35 => 4,  31 => 2,  19 => 1,);
    }
}
