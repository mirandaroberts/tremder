<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dob', 'ip_joined', 'sticks', 'bound_gems', 'confirmation_code'
    ];

    protected $dates = ['last_active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function chatMessages() {
      return $this->hasMany('App\ChatMessage', 'author_id');
    }

    public function wolves() {
      return $this->hasMany('App\Wolf');
    }
}
