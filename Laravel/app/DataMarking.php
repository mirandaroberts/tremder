<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataMarking extends Model
{
  protected $table = 'data_markings';

  protected $fillable = [
      'name', 'beta_species_id', 'public', 'special'
  ];

  public function species() {
    return $this->belongsTo('App\DataSpecies', 'data_species_id');
  }
}
