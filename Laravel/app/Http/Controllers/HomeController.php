<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function activate($token) {
      $user = Auth::user();

      if ($token == $user->confirmation_code) {
        $user->confirmed = 1;
        $user->confirmation_code = NULL;
        $user->save();

        Session::flash('message', 'Your account has been activated!');
        Session::flash('alert-class', 'alert-success');

        return redirect('/home');
      } else {
        Session::flash('message', 'Oops! Something went wrong.');
        Session::flash('alert-class', 'alert-danger');

        return redirect('/home');
      }
    }
}
