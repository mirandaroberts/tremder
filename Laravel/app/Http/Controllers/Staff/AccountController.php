<?php

namespace App\Http\Controllers\Staff;

use App\User;
use App\BetaCode;
use Mail;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('staff');
    }

    // SHOW BETA KEY GENERATOR
    public function getBetaCode()
    {
      return view('staff.createBetaCode');
    }

    // CREATE BETA CODE AND SEND EMAIL
    public function postBetaCode(Requests\BetaCodeRequest $request)
    {
      // Check if email already exists or has been sent a code
      $userExists = User::where('email', $request->email)->count();
      if ($userExists) {
        Session::flash('message', 'That email is already registered!');
        Session::flash('alert-class', 'alert-danger');

        return view('staff.createBetaCode');
      }

      $codeExists = BetaCode::where('email', $request->email)->count();
      if ($codeExists) {
        Session::flash('message', 'That email has already been sent a code!');
        Session::flash('alert-class', 'alert-danger');

        return view('staff.createBetaCode');
      }

      // Begin creating code information
      $betaCode = new BetaCode($request->all());

      // Create beta code
      $chars = "abcdefghijkmnopqrstuvwxyz023456789";
      srand((double)microtime()*1000000);
      $i = 0;
      $code = '';

      while ($i <= 7) {
          $num = rand() % 33;
          $tmp = substr($chars, $num, 1);
          $code = $code . $tmp;
          $i++;
      }

      $betaCode->code = $code;
      $betaCode->save();

      Mail::send('emails.beta', compact('betaCode', 'code'), function ($message) use ($betaCode) {
          $message->from('noreply@tremder.com', 'Tremder');
          $message->to($betaCode->email)->subject('Your Tremder Beta Access Code!');
      });

      Session::flash('message', 'You have sent a beta code to ' . $betaCode->email);
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/betacodes');
    }

    // MANAGE BETA CODES
    public function showBetaCodes() {
      $codes = BetaCode::all();

      return view('staff.manageBetaCode', compact('codes'));
    }

    // RESEND BETA CODE
    public function resendBetaCode($id) {
      $betaCode = BetaCode::findOrFail($id);

      Mail::send('emails.beta', compact('betaCode', 'code'), function ($message) use ($betaCode) {
          $message->from('noreply@tremder.com', 'Tremder');
          $message->to($betaCode->email)->subject('Your Tremder Beta Access Code!');
      });

      Session::flash('message', 'You have resent a beta code to ' . $betaCode->email);
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/betacodes');
    }

    // DELETE BETA CODE
    public function deleteBetaCode($id) {
      $betaCode = BetaCode::findOrFail($id);
      $betaCode->delete();

      Session::flash('message', 'You have deleted a beta code for ' . $betaCode->email);
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/betacodes');
    }
}
