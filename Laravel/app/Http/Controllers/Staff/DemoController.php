<?php

namespace App\Http\Controllers\Staff;

use App\DataSpecies;
use App\DataMarking;
use App\DataEyeColor;
use App\DataBaseColor;
use App\DataMutation;
use App\Http\Requests;
use App\Classes\PetGenerator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DemoController extends Controller
{
    /**
     * Show the staff demo selection page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showDemoSelect() {
      $species = DataSpecies::all();

      return view('staff.demoSelect', compact('species'));
    }

    /**
     * Show the staff demo.
     *
     * @return \Illuminate\Http\Response
     */
     public function showDemo($s) {
       $species = DataSpecies::where('name', $s)->firstOrFail();
       $img = new PetGenerator();

       ob_start();
        $output =  $img->drawPet($species->name, 'male', 730, 1, 1, array());
       ob_end_clean();

       return view('staff.demo', compact('species', 'output'));
     }

     /**
      * Process demo data
      *
      * @return \Illuminate\Http\Response
      */
      public function processPet(Request $request) {
        // Function to combine 3 arrays
       function combine($arr1, $arr2, $arr3) {
         $out = array();

         $arr1 = array_values($arr1);
         $arr2 = array_values($arr2);
         $arr3 = array_values($arr3);

         foreach ($arr1 AS $key1 => $value1) {
           $out[(string)$value1][0] = $arr2[$key1];
           $out[(string)$value1][1] = $arr3[$key1];
         }

         return $out;
      }

        $species = DataSpecies::where('name', $request->species)->firstOrFail();
        $img = new PetGenerator();

        $aMarkings = array();

        if ($request->markings) {
          foreach ($request->markings as $value) {
            foreach ($value as $key => $inner) {
              if ($inner != 'markings[]') {
                $aMarkings[] = $inner;
              }
            }
          }

          $aColors = array();

          foreach ($request->markingColors as $value) {
            foreach ($value as $key => $inner) {
              if ($inner != 'markingColors[]') {
                $aColors[] = $inner;
              }
            }
          }

          $aOpacities = array();

          foreach ($request->markingOpacity as $value) {
            foreach ($value as $key => $inner) {
              if ($inner != 'markingOpacity[]') {
                $aOpacities[] = $inner;
              }
            }
          }

          // Create marking array
          $markings = combine($aMarkings, $aColors, $aOpacities);
        } else {
          $markings = array();
        }

        ob_start();
          $output = $img->drawPet($species->name, $request->gender, $request->age, $request->eyeColor, $request->baseColor, $markings);
        ob_end_clean();

        return json_encode(base64_encode($output));
      }

      public function identifyMarking(Request $request) {
        $marking = DataMarking::findOrFail($request->Marking);

        $data = array('special' => $marking->special, 'palette' => $marking->palette);
        return json_encode($data);
      }
}
