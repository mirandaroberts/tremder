<?php

namespace App\Http\Controllers\Staff;

use App\DataSpecies;
use App\DataMarking;
use App\DataEyeColor;
use App\DataBaseColor;
use App\DataMutation;
use App\DataColor;
use Session;
use File;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('staff');
    }

    /**
 	 * Show form to create species
 	 *
 	 * @return view
	 */

    public function createSpecies() {
      return view('staff.createSpecies');
    }

    /**
 	 * Create species
 	 *
 	 * @param array
 	 * @return void
	 */

    public function postCreateSpecies(Requests\CreateSpeciesRequest $request) {
      // Create database entry
      $species = new DataSpecies($request->all());

      // Parse name
      $name = strtolower(preg_replace('/\s?/', '', $species->name));
      $species->name = $name;
      $species->save();

      // Create directories
      File::makeDirectory('images/assets/bases/' . $species->name, 0755, true);
      File::makeDirectory('images/assets/bases/' . $species->name . '/male', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/male/aging', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/male/eyes', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/male/markings', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/male/mutations', 0755, true);
      File::makeDirectory('images/assets/bases/' . $species->name . '/female', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/female/aging', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/female/eyes', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/female/markings', 0755, true);
        File::makeDirectory('images/assets/bases/' . $species->name . '/female/mutations', 0755, true);
      File::makeDirectory('images/assets/bases/' . $species->name . '/baby', 0755, true);

      Session::flash('message', $species->name . ' has been created!');
      Session::flash('alert-class', 'alert-success');

      return redirect('/species/edit/' . $species->name);
    }

    /**
 	 * Show manage spcecies view
 	 *
 	 * @return view
	 */

    public function showSpecies() {
      $species = DataSpecies::all();

      return view('staff.manageSpecies', compact('species'));
    }

    /**
 	 * Manage a specific pet's files
 	 *
   * @param string
 	 * @return view
	 */
    public function editSpecies($name) {
      $species = DataSpecies::where('name', $name)->firstOrFail();

      // Check to see if files exist
      $missing = array();
      $male = array();
      $female = array();
      $baby = array();

      // Male
      if (!File::exists('images/assets/bases/' . $species->name . '/male/lineart.png')) {
        $male[] = 'Lineart';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/nose_default.png')) {
        $male[] = 'Default Nose';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/10_years.png')) {
        $male[] = 'Aging: 10 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/7.5_years.png')) {
        $male[] = 'Aging: 7.5 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/7_years.png')) {
        $male[] = 'Aging: 7 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/8.5_years.png')) {
        $male[] = 'Aging: 8.5 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/8_years.png')) {
        $male[] = 'Aging: 8 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/9.5_years.png')) {
        $male[] = 'Aging: 9.5 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/male/aging/9_years.png')) {
        $male[] = 'Aging: 9 years';
      }

      foreach ($species->markings as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/male/markings/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $male[] = 'Marking: ' . $m->name;
        }
      }

      foreach ($species->baseColors as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/male/base_' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $male[] = 'Base: ' . $m->name;
        }
      }

      foreach ($species->mutations as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/male/mutations/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $male[] = 'Mutation: ' . $m->name;
        }
      }

      foreach ($species->eyeColors as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/male/eyes/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $male[] = 'Eyes: ' . $m->name;
        }
      }

      // Female
      if (!File::exists('images/assets/bases/' . $species->name . '/female/lineart.png')) {
        $female[] = 'Lineart';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/nose_default.png')) {
        $female[] = 'Default Nose';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/10_years.png')) {
        $female[] = 'Aging: 10 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/7.5_years.png')) {
        $female[] = 'Aging: 7.5 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/7_years.png')) {
        $female[] = 'Aging: 7 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/8.5_years.png')) {
        $female[] = 'Aging: 8.5 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/8_years.png')) {
        $female[] = 'Aging: 8 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/9.5_years.png')) {
        $female[] = 'Aging: 9.5 years';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/female/aging/9_years.png')) {
        $female[] = 'Aging: 9 years';
      }

      foreach ($species->markings as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/female/markings/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $female[] = 'Marking: ' . $m->name;
        }
      }

      foreach ($species->baseColors as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/female/base_' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $female[] = 'Base: ' . $m->name;
        }
      }

      foreach ($species->mutations as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/female/mutations/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $female[] = 'Mutation: ' . $m->name;
        }
      }

      foreach ($species->eyeColors as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/female/eyes/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $female[] = 'Eyes: ' . $m->name;
        }
      }

      // Baby
      if (!File::exists('images/assets/bases/' . $species->name . '/baby/lineart.png')) {
        $baby[] = 'Lineart';
      }

      if (!File::exists('images/assets/bases/' . $species->name . '/baby/default.png')) {
        $baby[] = 'Default Color';
      }

      foreach ($species->mutations as $m) {
        if (!File::exists('images/assets/bases/' . $species->name . '/baby/mutations/' . strtolower(str_replace(' ', '_', $m->name)) . '.png')) {
          $baby[] = 'Mutation: ' . $m->name;
        }
      }

      $missing['female'] = $female;
      $missing['male'] = $male;
      $missing['baby'] = $baby;

      return view('staff.editSpecies', compact('species', 'missing'));
    }

    /**
 	 * Upload files and update pet
 	 *
 	 * @param type
 	 * @return void
	 */
   public function postEditSpecies($species, Request $request) {
     // Set Validator
     $v = Validator::make($request->all(), [
       'items.*' => 'image|mimes:png'
     ]);

     if ($v->fails()) {
       Session::flash('message', 'You can only upload png files using this page!');
       Session::flash('alert-class', 'alert-danger');

       return redirect('/staff/species/edit/' . $species->name);
     }

     // Get species info
     $species = DataSpecies::where('name', $species)->firstOrFail();

     // Save the files to the database
     // Male lineart
     if ($request->items['m_lineart']) {
       $file = $request->items['m_lineart'];
       $name = 'lineart.png';
       $path = 'images/assets/bases/' . $species->name . '/male';
       $file->move($path, $name);
     }

     // Female lineart
     if ($request->items['f_lineart']) {
       $file = $request->items['f_lineart'];
       $name = 'lineart.png';
       $path = 'images/assets/bases/' . $species->name . '/female';
       $file->move($path, $name);
     }

     // Baby lineart
     if ($request->items['b_lineart']) {
       $file = $request->items['b_lineart'];
       $name = 'lineart.png';
       $path = 'images/assets/bases/' . $species->name . '/baby';
       $file->move($path, $name);
     }

     // Male default nose
     if ($request->items['m_base_nose']) {
       $file = $request->items['m_base_nose'];
       $name = 'nose_default.png';
       $path = 'images/assets/bases/' . $species->name . '/male';
       $file->move($path, $name);
     }

     // Female Default nose
     if ($request->items['f_base_nose']) {
       $file = $request->items['f_base_nose'];
       $name = 'nose_default.png';
       $path = 'images/assets/bases/' . $species->name . '/female';
       $file->move($path, $name);
     }

     // Baby base color
     if ($request->items['b_base_color']) {
       $file = $request->items['b_base_color'];
       $name = 'default.png';
       $path = 'images/assets/bases/' . $species->name . '/baby';
       $file->move($path, $name);
     }

     // Eye Colors
     foreach ($request->items['eye.m.*'] as $k) {
       $file = $k;
       $name = $k->getClientOriginalName();
       $path = 'images/assets/bases/' . $species->name . '/male/eyes';
       $file->move($path, $name);
     }

     Session::flash('message', $species->name . ' has been edited!');
     Session::flash('alert-class', 'alert-success');

     return redirect('/staff/species/edit/' . $species->name);
   }

    /**
 	 * Show page to create a marking
 	 *
 	 * @return view
	 */
    public function createMarking() {
      $species = DataSpecies::all();

      return view('staff.createMarking', compact('species'));
    }

    /**
 	 * Create and validate marking
 	 *
   * @param array
 	 * @return redirect
	 */
    public function postCreateMarking(Requests\CreateMarkingRequest $request) {
      $marking = new DataMarking($request->all());
      $marking->data_species_id = $request->data_species_id;
      $marking->special = $request->special;
      $marking->save();

      Session::flash('message', $marking->name . ' has been created!');
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/markings');
    }

    /**
 	 * Show all markings
 	 *
 	 * @return view
	 */
    public function showMarkings() {
      $species = DataSpecies::with('markings')->get();

      return view('staff.manageMarkings', compact('species'));
    }

    /**
   * Show page to create a base color
   *
   * @return view
   */
    public function createBaseColor() {
      $species = DataSpecies::all();

      return view('staff.createBaseColor', compact('species'));
    }

    /**
 	 * Create and validate base color
 	 *
   * @param array
 	 * @return redirect
	 */
    public function postCreateBaseColor(Requests\BaseColorRequest $request) {
      $color = new DataBaseColor($request->all());
      $color->data_species_id = $request->data_species_id;
      $color->special = $request->special;
      $color->save();

      Session::flash('message', $color->name . ' has been created!');
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/basecolors');
    }

    /**
 	 * Show page to create a mutation
 	 *
 	 * @return view
	 */
    public function createMutation() {
      $species = DataSpecies::all();

      return view('staff.createMutation', compact('species'));
    }

    /**
 	 * Create and validate mutation
 	 *
   * @param array
 	 * @return redirect
	 */
    public function postCreateMutation(Requests\CreateMutationRequest $request) {
      $mutation = new DataMutation($request->all());
      $mutation->data_species_id = $request->data_species_id;
      $mutation->save();

      Session::flash('message', $mutation->name . ' has been created!');
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/mutations');
    }

    /**
 	 * Show page to create eye color
 	 *
 	 * @return view
	 */
    public function createEyeColor() {
      $species = DataSpecies::all();

      return view('staff.createEyeColor', compact('species'));
    }

    /**
 	 * Create and validate eye color
 	 *
   * @param array
 	 * @return redirect
	 */
    public function postCreateEyeColor(Requests\EyeColorRequest $request) {
      $color = new DataEyeColor($request->all());
      $color->data_species_id = $request->data_species_id;
      $color->save();

      Session::flash('message', $color->name . ' has been created!');
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff/eyecolors');
    }

    /**
   * Show page to create palette color
   *
   * @return view
   */
    public function createColor() {
      $species = DataSpecies::all();

      return view('staff.createColor', compact('species'));
    }

    /**
   * Create and validate eye color
   *
   * @param array
   * @return redirect
   */
    public function postCreateColor(Requests\CreateColorRequest $request) {
      $color = new DataColor($request->all());
      $color->save();

      Session::flash('message', $color->name . ' has been created!');
      Session::flash('alert-class', 'alert-success');

      return redirect('/staff');
    }
}
