<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\ChatGuest;
use App\DataSpecies;
use App\DataMarking;
use App\Classes\PetGenerator;
use Auth;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function demo() {
      $species = DataSpecies::where('name', 'wolf')->firstOrFail();
      $img = new PetGenerator();

      ob_start();
       $output =  $img->drawPet($species->name, 'male', 730, 1, 1, array());
      ob_end_clean();

      return view('demo', compact('species', 'output'));
    }

    /**
     * Process demo data
     *
     * @return \Illuminate\Http\Response
     */
     public function processPet(Request $request) {
       // Function to combine 3 arrays
      function combine($arr1, $arr2, $arr3) {
        $out = array();

        $arr1 = array_values($arr1);
        $arr2 = array_values($arr2);
        $arr3 = array_values($arr3);

        foreach ($arr1 AS $key1 => $value1) {
          $out[(string)$value1][0] = $arr2[$key1];
          $out[(string)$value1][1] = $arr3[$key1];
        }

        return $out;
     }

       $species = DataSpecies::where('name', 'wolf')->firstOrFail();
       $img = new PetGenerator();

       $aMarkings = array();

       if ($request->markings) {
         foreach ($request->markings as $value) {
           foreach ($value as $key => $inner) {
             if ($inner != 'markings[]') {
               $aMarkings[] = $inner;
             }
           }
         }

         $aColors = array();

         foreach ($request->markingColors as $value) {
           foreach ($value as $key => $inner) {
             if ($inner != 'markingColors[]') {
               $aColors[] = $inner;
             }
           }
         }

         $aOpacities = array();

         foreach ($request->markingOpacity as $value) {
           foreach ($value as $key => $inner) {
             if ($inner != 'markingOpacity[]') {
               $aOpacities[] = $inner;
             }
           }
         }

         // Create marking array
         $markings = combine($aMarkings, $aColors, $aOpacities);
       } else {
         $markings = array();
       }

       ob_start();
         $output = $img->drawPet($species->name, 'male', $request->age, $request->eyeColor, $request->baseColor, $markings);
       ob_end_clean();

       return json_encode(base64_encode($output));
     }

     public function identifyMarking(Request $request) {
       $marking = DataMarking::findOrFail($request->Marking);

       $data = array('special' => $marking->special, 'palette' => $marking->palette);
       return json_encode($data);
     }

    public function updateChat() {
      $messagesList = ChatMessage::idDescending()->get();
      $messages = array();

      foreach ($messagesList as $message) {
        if ($message->guest_id == 0) {
          $m['author'] = $message->author->name;
          $m['body'] = $message->chat_body;
          $m['ts'] = $message->created_at->diffForHumans();

          $messages[$message->id] = $m;
        } else {
          $m['author'] = 'Guest';
          $m['body'] = $message->chat_body;
          $m['ts'] = $message->created_at->diffForHumans();

          $messages[$message->id] = $m;
        }
      }

      $data = array('messages' => $messages);
      return json_encode($data);
    }

    public function postChat(Request $request) {
      // Validate message
      if (!$request->chat_body) {
        $data = array('type' => 'error', 'message' => 'You must type something first!');
        return json_encode($data);
      } else if (strlen($request->chat_body) > 225) {
        $data = array('type' => 'error', 'message' => 'Your message is too long!');
        return json_encode($data);
      }

      if (!Auth::user()) {
        // Check if guest id exists
        $guestCheck = ChatGuest::where('ip', $_SERVER['REMOTE_ADDR'])->first();

        if ($guestCheck) {
          $post = new ChatMessage($request->all());
          $post->guest_id = $guestCheck->id;
          $post->chat_body = $request->chat_body;
          $post->save();
        } else {
          $guest = new ChatGuest();
          $guest->ip = $_SERVER['REMOTE_ADDR'];
          $guest->save();

          $post = new ChatMessage($request->all());
          $post->guest_id = $guestCheck->id;
          $post->chat_body = $request->chat_body;
          $post->save();
        }
      } else {
        $post = new ChatMessage($request->all());
        $post->author_id = Auth::user()->id;
        $post->chat_body = $request->chat_body;
        $post->save();
      }

      return json_encode('SUCCESS');
    }
}
