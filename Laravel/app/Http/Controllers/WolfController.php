<?php

namespace App\Http\Controllers;

use App\Wolf;
use App\WolfMarking;
use App\DataBaseColor;
use App\DataColor;
use App\DataEyeColor;
use App\DataMarking;
use App\DataMutations;
use App\User;
use Carbon\Carbon;
use DB;
use Auth;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Classes\PetGenerator;

class WolfController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function myTerritory() {
        return view('myTerritory');
    }

    public function firstWolves() {
        // Check if user has wolves
        if (Auth::user()->wolves->count() > 0) {
            Session::flash('message', 'You already have gotten wolves!');
            Session::flash('alert-class', 'alert-danger');

            return redirect('/territory');
        }

        // Create male
        $male = new Wolf();
        $male->name = 'Male Wolf';
        $male->user_id = Auth::user()->id;
        $male->gender = 'male';
        $male->age = 730;
        $male->primary = 1;
        $male->health = 'Amazing';
        $male->data_base_color_id = DataBaseColor::where('special', NULL)->orderBy(\DB::raw('RAND()'))->value('id');
        $male->data_eye_color_id = DataEyeColor::orderBy(\DB::raw('RAND()'))->value('id');
        $male->save();

        // generate markings
        $num = rand(5,10); // How many markings we'll have
        $exclude = array(); // variable that will prevent the same marking from being repeated

        for ($x = 0; $x <= $num; $x++) {
            $marking = new WolfMarking();
            $marking->wolf_id = $male->id;
            $marking->marking_id = DataMarking::whereNotIn('id', $exclude)->where('special', NULL)->where('palette', 0)->orderBy(\DB::raw('RAND()'))->value('id');
            $marking->color_id = DataColor::where('special', NULL)->orderBy(\DB::raw('RAND()'))->value('id');
            $marking->opacity = 1;
            $marking->order = $x;
            $marking->save();

            // Set marking in exclude array so it won't select it in next loop
            $exclude[] = $marking->marking_id;
        }

        // Create female
        $female = new Wolf();
        $female->name = 'Female Wolf';
        $female->user_id = Auth::user()->id;
        $female->gender = 'female';
        $female->age = 730;
        $female->primary = 1;
        $female->health = 'Amazing';
        $female->data_base_color_id = DataBaseColor::where('special', NULL)->orderBy(\DB::raw('RAND()'))->value('id');
        $female->data_eye_color_id = DataEyeColor::orderBy(\DB::raw('RAND()'))->value('id');
        $female->save();

        // generate markings
        $num = rand(5,10); // How many markings we'll have
        $exclude = array(); // variable that will prevent the same marking from being repeated

        for ($x = 0; $x <= $num; $x++) {
            $marking = new WolfMarking();
            $marking->wolf_id = $female->id;
            $marking->marking_id = DataMarking::whereNotIn('id', $exclude)->where('special', NULL)->where('palette', 0)->orderBy(\DB::raw('RAND()'))->value('id');
            $marking->color_id = DataColor::where('special', NULL)->orderBy(\DB::raw('RAND()'))->value('id');
            $marking->opacity = 1;
            $marking->order = $x;
            $marking->save();

            // Set marking in exclude array so it won't select it in next loop
            $exclude[] = $marking->marking_id;
        }

        // Generate images
        $img = new PetGenerator();
        $img->updatePet($male->id);
        $img->updatePet($female->id);

        return redirect('/territory');
    }
}
