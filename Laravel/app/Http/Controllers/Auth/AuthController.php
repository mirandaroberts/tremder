<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\BetaCode;
use Validator;
use Session;
use Auth;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $loginView = 'welcome';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'activate']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'        => 'required|max:20|unique:users',
            'email'       => 'required|email|max:100|unique:users',
            'password'    => 'required|confirmed|min:6',
            'dob'         => 'required|before:13 years ago',
            'code'        => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
      // create confirmation code
      $hash = md5(uniqid(rand(), true));

      return User::create([
          'name'              => $data['name'],
          'email'             => $data['email'],
          'dob'               => $data['dob'],
          'ip_joined'         => $_SERVER['REMOTE_ADDR'],
          'password'          => bcrypt($data['password']),
          'sticks'            => 3000,
          'bound_gems'        => 10,
          'confirmation_code' => $hash
      ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        // Validates and voids beta code
        $checkCode = BetaCode::where('code', $request->code)->first();
        if ($checkCode == NULL) {
          Session::flash('message', 'Beta code is invalid!');
          Session::flash('alert-class', 'alert-danger');

          return redirect('/register');
        }

        // If code exists, we check email and create account
        if ($checkCode->email == $request->email) {
            $checkCode->code = NULL;
            $checkCode->save();

            Auth::login($this->create($request->all()));
            $user = Auth::user();

            // Send confirmation email
            Mail::send('auth.emails.confirmation', ['user' => $user], function ($m) use ($user) {
              $m->from('noreply@tremder.com', 'Tremder');
              $m->to($user->email, $user->name)->subject('Activate your Tremder Account!');
            });

            return redirect('/home');
        } else {
          Session::flash('message', 'Beta code is invalid!');
          Session::flash('alert-class', 'alert-danger');

          return redirect('/register');
        }
    }
}
