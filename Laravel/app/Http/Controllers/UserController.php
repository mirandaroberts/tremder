<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Auth;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile($id) {
        $user = User::findOrFail($id);
        $now = Carbon::now()->subMinutes(5);

        return view('profile', compact('user', 'now'));
    }

    public function editProfile() {
        return view('editProfile');
    }

    public function postEditProfile(Requests\EditProfileRequest $request) {
        // validate avatar
        if ($request->avatar) {
            $file = $request->avatar;
            $name = Auth::user()->id . '.png';
            $path = 'images/avatars/';
            $file->move($path, $name);
        }

        if (!Auth::user()->avatar) {
            Auth::user()->avatar = 1;
        }

        Auth::user()->bio = $request->bio;
        Auth::user()->save();

        Session::flash('message', 'You have edited your profile!');
        Session::flash('alert-class', 'alert-success');

        return redirect('/profile/' . Auth::user()->id);
    }
}
