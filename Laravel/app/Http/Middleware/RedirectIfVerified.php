<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfVerified
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->confirmed != 1) {
            return redirect('home');
        }

        return $next($request);
    }

}
