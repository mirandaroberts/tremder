<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class Staff
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (isset($request->user()->permissions) != 1) {
          Session::flash('message', 'You are not authorized to do that!');
          Session::flash('alert-class', 'alert-danger');

          return redirect('/');
        }

        return $next($request);
    }

}
