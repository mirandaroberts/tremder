<?php namespace App\Http\Requests;

use Config;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\Request;

class CreateMutationRequest extends Request {

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'name' => 'required|max:20|unique:data_mutations',
      'data_species_id' => 'required'
    ];
  }

}
