<?php namespace App\Http\Requests;

use Config;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\Request;

class CreateColorRequest extends Request {

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'name' => 'required|max:50|unique:data_colors',
      'data_species_id' => 'required',
      'color' => 'required|max:6|min:6'
    ];
  }

}
