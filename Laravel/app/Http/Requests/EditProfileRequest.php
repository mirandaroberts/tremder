<?php namespace App\Http\Requests;

use Config;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\Request;

class EditProfileRequest extends Request {

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'bio'    => 'max:1000',
      'avatar' => 'image|mimes:png|max:500'
    ];
  }

}
