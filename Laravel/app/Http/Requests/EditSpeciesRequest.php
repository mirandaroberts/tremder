<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditSpeciesRequest extends Request {

  public function authorize()
  {
    return true;
  }

  public function rules() {
    foreach($this->request->get('items') as $key => $val) {
      $rules['items.' . $key] = 'image|mimes:png';
    }

    return $rules;
  }
}
