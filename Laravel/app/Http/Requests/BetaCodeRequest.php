<?php namespace App\Http\Requests;

use Config;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\Request;

class BetaCodeRequest extends Request {

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'email' => 'required|unique:beta_codes',
    ];
  }

}
