<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatGuest extends Model
{
    public function messages()
    {
        return $this->hasMany('App\ChatMessage', 'guest_id');
    }
}
