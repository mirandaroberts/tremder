<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataEyeColor extends Model
{
  protected $table = 'data_eye_colors';

  protected $fillable = [
      'name', 'beta_species_id', 'public'
  ];

  public function species() {
    return $this->belongsTo('App\DataSpecies', 'data_species_id');
  }
}
