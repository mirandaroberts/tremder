<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSpecies extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'data_species';

    protected $fillable = [
        'name'
    ];

    // Relationships
    public function markings()
    {
        return $this->hasMany('App\DataMarking')->orderBy('name', 'asc');
    }

    public function publicMarkings()
    {
        return $this->hasMany('App\DataMarking')->where('public', 1)->orderBy('name', 'asc');
    }

    public function eyeColors()
    {
        return $this->hasMany('App\DataEyeColor');
    }

    public function baseColors()
    {
        return $this->hasMany('App\DataBaseColor');
    }

    public function publicBaseColors()
    {
        return $this->hasMany('App\DataBaseColor')->where('public', 1);
    }

    public function mutations()
    {
        return $this->hasMany('App\DataMutation');
    }

    public function colors()
    {
        return $this->hasMany('App\DataColor');
    }
}
