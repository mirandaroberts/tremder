<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wolf extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wolves';
    protected $dates = ['due_date'];
    protected $fillable = ['name', 'gender', 'age', 'base_color', 'eye_color', 'mutation'];

    // Relationships
    public function markings()
    {
        return $this->hasMany('App\WolfMarking')->orderBy('order');
    }

    public function baseColor()
    {
        return $this->hasOne('App\DataBaseColor');
    }

    public function eyeColor()
    {
        return $this->hasOne('App\DataEyeColor');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
