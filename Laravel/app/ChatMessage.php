<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $fillable = ['body'];

    public function scopeIdDescending($query) {
      return $query->orderBy('id','DESC');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function guestAuthor()
    {
        return $this->hasMany('App\ChatGuest', 'guest_id');
    }
}
