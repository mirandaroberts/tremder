<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataBaseColor extends Model
{
  protected $table = 'data_base_colors';

  protected $fillable = [
      'name', 'beta_species_id', 'public', 'special'
  ];

  public function species() {
    return $this->belongsTo('App\DataSpecies', 'data_species_id');
  }
}
