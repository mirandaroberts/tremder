<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataMutation extends Model
{
  protected $table = 'data_mutations';

  protected $fillable = [
      'name', 'beta_species_id', 'public'
  ];

  public function species() {
    return $this->belongsTo('App\DataSpecies', 'data_species_id');
  }
}
