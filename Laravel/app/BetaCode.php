<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetaCode extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beta_codes';

    protected $fillable = [
        'email'
    ];
}
