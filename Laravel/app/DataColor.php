<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataColor extends Model
{
  protected $table = 'data_colors';

  protected $fillable = [
      'name', 'data_species_id', 'public', 'special', 'color'
  ];

  public function species() {
    return $this->belongsTo('App\DataSpecies', 'data_species_id');
  }
}
