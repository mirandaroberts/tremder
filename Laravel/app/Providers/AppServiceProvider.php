<?php

namespace App\Providers;

use Auth;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        view()->composer('*', function($view) {
          if (Auth::user()) {
            // Update last online field
            Auth::user()->last_active = Carbon::now();
            Auth::user()->last_active_ip = $_SERVER['REMOTE_ADDR'];
            Auth::user()->save();
          }
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
