<?php namespace App\Classes;
/**
 * For use only by Tremder.
 * Creator: Miranda Roberts
 *
 *
 * Custom pet generator.
 */

use \Imagick;
use File;
use App\DataSpecies;
use App\DataMarking;
use App\DataEyeColor;
use App\DataBaseColor;
use App\DataMutation;
use App\DataColor;
use App\Wolf;
use App\WolfMarking;

class PetGenerator {
  public function drawPet($species, $gender, $age, $eyeColor, $baseColor, $markings = array(), $mutation = NULL, $background = 'default') {
    // Get species data
    $data = DataSpecies::where('name', $species)->firstOrFail();

    // Get background data
    if ($background == 'default') {
      $background = new \Imagick('images/assets/backgrounds/default.png');
    } else {
      // TODO: pull background from db and instantiate image
    }

    // If age is equal to or greater than 730...
    if ($age >= 730) {
      // Instantiate adult objects
      $lineart = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/lineart.png');
      $nose = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/nose_default.png');

      $eyeData = DataEyeColor::findOrFail($eyeColor);
      $eye = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/eyes/' . strtolower(str_replace(" ", "_", $eyeData->name)) . '.png');

      $baseData = DataBaseColor::findOrFail($baseColor);
      $base = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/base_' . strtolower(str_replace(" ", "_", $baseData->name)) . '.png');

      // Aging effects
      if ($age < 2555) {
        $aging = NULL;
      }

      if ($age >= 2555) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/7_years.png');
      }

      if ($age >= 2738) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/7.5_years.png');
      }

      if ($age >= 2920) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/8_years.png');
      }

      if ($age >= 3103) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/8.5_years.png');
      }

      if ($age >= 3285) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/9_years.png');
      }

      if ($age >= 3468) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/9.5_years.png');
      }

      if ($age >= 3650) {
        $aging = new \Imagick('images/assets/bases/' . $data->name . '/' . $gender . '/aging/10_years.png');
      }
    } else {
      $lineart = new \Imagick('images/assets/bases/' . $data->name . '/baby/lineart.png');
      $base = new \Imagick('images/assets/bases/' . $data->name . '/baby/default.png');
    }

    // Composite images
    $background->compositeImage($base, imagick::COMPOSITE_DEFAULT, 0, 0);


    if ($age >= 730) {
      $background->compositeImage($eye, imagick::COMPOSITE_DEFAULT, 0, 0);
      $background->compositeImage($nose, imagick::COMPOSITE_DEFAULT, 0, 0);

      // Add Markings
      foreach ($markings as $marking => $inner) {
        $markingData = DataMarking::findOrFail($marking);
        $colorData = DataColor::findOrFail($inner[0]);

        $m = new \Imagick("images/assets/bases/" . $species . "/" . $gender . "/markings/" . strtolower(str_replace(" ", "_", $markingData->name)) . ".png");
        $m->evaluateImage(Imagick::EVALUATE_MULTIPLY, $inner[1], Imagick::CHANNEL_ALPHA); // Set opactiy

        if ($markingData->special) {
          $background->compositeImage($m, imagick::COMPOSITE_DEFAULT, 0, 0);
        } else {
          $m->setImageAlphaChannel(Imagick::ALPHACHANNEL_EXTRACT);
          $m->setImageBackgroundColor('#' . $colorData->color);
          $m->setImageAlphaChannel(Imagick::ALPHACHANNEL_SHAPE);
          $background->compositeImage($m, imagick::COMPOSITE_DEFAULT, 0, 0);
        }
      }

      if ($aging) {
        $background->compositeImage($aging, imagick::COMPOSITE_DEFAULT, 0, 0);
      }
    }

    $background->compositeImage($lineart, imagick::COMPOSITE_DEFAULT, 0, 0);

    // Outputs final result
    $background->setImageColorSpace(imagick::COLORSPACE_SRGB);
    return $background;
  }

  public function updatePet($id) {
      $pet = Wolf::findOrFail($id);
      $background = 'default';
      $species = 'wolf';

      // Get background data
      if ($background == 'default') {
        $background = new \Imagick('images/assets/backgrounds/default.png');
      } else {
        // TODO: pull background from db and instantiate image
      }

      // If age is equal to or greater than 730...
      if ($pet->age >= 730) {
        // Instantiate adult objects
        $lineart = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/lineart.png');
        $nose = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/nose_default.png');

        $eyeData = DataEyeColor::findOrFail($pet->data_eye_color_id);
        $eye = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/eyes/' . strtolower(str_replace(" ", "_", $eyeData->name)) . '.png');

        $baseData = DataBaseColor::findOrFail($pet->data_base_color_id);
        $base = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/base_' . strtolower(str_replace(" ", "_", $baseData->name)) . '.png');

        // Aging effects
        if ($pet->age < 2555) {
          $aging = NULL;
        }

        if ($pet->age >= 2555) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/aging/7_years.png');
        }

        if ($pet->age >= 2738) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/aging/7.5_years.png');
        }

        if ($pet->age >= 2920) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/aging/8_years.png');
        }

        if ($pet->age >= 3103) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/aging/8.5_years.png');
        }

        if ($pet->age >= 3285) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/aging/9_years.png');
        }

        if ($pet->age >= 3468) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $pet->gender . '/aging/9.5_years.png');
        }

        if ($pet->age >= 3650) {
          $aging = new \Imagick('images/assets/bases/' . $species . '/' . $gender . '/aging/10_years.png');
        }
      } else {
        $lineart = new \Imagick('images/assets/bases/' . $species . '/baby/lineart.png');
        $base = new \Imagick('images/assets/bases/' . $species . '/baby/default.png');
      }

      // Composite images
      $background->compositeImage($base, imagick::COMPOSITE_DEFAULT, 0, 0);


      if ($pet->age >= 730) {
        $background->compositeImage($eye, imagick::COMPOSITE_DEFAULT, 0, 0);
        $background->compositeImage($nose, imagick::COMPOSITE_DEFAULT, 0, 0);

        // Add Markings
        foreach ($pet->markings as $marking) {
          $markingData = DataMarking::findOrFail($marking->marking_id);
          $colorData = DataColor::findOrFail($marking->color_id);

          $m = new \Imagick("images/assets/bases/" . $species . "/" . $pet->gender . "/markings/" . strtolower(str_replace(" ", "_", $markingData->name)) . ".png");
          $m->evaluateImage(Imagick::EVALUATE_MULTIPLY, $marking->opacity, Imagick::CHANNEL_ALPHA); // Set opactiy

          if ($markingData->special) {
            $background->compositeImage($m, imagick::COMPOSITE_DEFAULT, 0, 0);
          } else {
            $m->setImageAlphaChannel(Imagick::ALPHACHANNEL_EXTRACT);
            $m->setImageBackgroundColor('#' . $colorData->color);
            $m->setImageAlphaChannel(Imagick::ALPHACHANNEL_SHAPE);
            $background->compositeImage($m, imagick::COMPOSITE_DEFAULT, 0, 0);
          }
        }

        if ($aging) {
          $background->compositeImage($aging, imagick::COMPOSITE_DEFAULT, 0, 0);
        }
      }

      $background->compositeImage($lineart, imagick::COMPOSITE_DEFAULT, 0, 0);

      // save image
      $background->setImageColorSpace(imagick::COLORSPACE_SRGB);
      $background->writeImage('images/wolves/' . $pet->id . '.png');
      return true;
  }
}
