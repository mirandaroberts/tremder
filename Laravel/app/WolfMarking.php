<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WolfMarking extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [];

    // Relationships
    public function wolf()
    {
        return $this->belongsTo('App\Wolf');
    }
}
