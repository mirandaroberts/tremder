<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->date('dob');
            $table->string('ip_joined')
            $table->integer('sticks');
            $table->integer('bound_gems');
            $table->integer('unbound_gems');
            $table->integer('main_male');
            $table->integer('main_female');
            $table->string('territory_order', 20);
            $table->dateTime('frozen');
            $table->string('avatar', 100);
            $table->dateTime('last_active');
            $table->string('last_active_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
