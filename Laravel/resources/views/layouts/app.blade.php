<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Tremder - @yield('title')</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FAVICONS -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/normalize.min.css">
        <link rel="stylesheet" href="/css/main.css">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- MODERNIZER -->
        <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="header-container">
            <header class="wrapper clearfix">
                <h1 class="title"><a href="/">Tremder</a></h1>
                <nav>
                    <ul>
                        <li><a href="/territory">Territory</a></li>
                        <li><a href="#">Inventory</a></li>
                        <li><a href="#">Explore</a></li>
                        <li><a href="#">Socialize</a></li>
                        <li><a href="#">Forums</a></li>
                        <li><a href="#">Meadow</a></li>
                    </ul>
                </nav>
            </header>
        </div>

        <div class="main-container">
            <div class="main wrapper clearfix">

              <div id="content" class="left">
                <h1>@yield('header')</h1>

                @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif

                @yield('content')
              </div>

              <div id="sidebar">
                <aside>
                      @if (Auth::user())
                        <h3>Account</h3>

                        <div class="center">
                            Welcome, <a href="/profile/{{ Auth::user()->id }}">{{ Auth::user()->name }}</a> (#{{ Auth::user()->id }})
                            <br>
                            @if (Auth::user()->avatar)
                                <img src="/images/avatars/{{ Auth::user()->id }}.png" width="200" height="200" class="icon">
                            @else
                                <img src="/images/avatars/default.png" width="200" height="200" class="icon">
                            @endif

                            <br><span class="small-txt"><a href="/account">Account Options</a> || <a href="/editprofile">Edit Profile</a></span>
                        </div>

                        <div class="center small-txt">
                            <table class="currency center" width="100%">
                                <tr>
                                    <td><b>Sticks</b></td>
                                    <td>{{ Auth::user()->sticks }}</td>
                                </tr>
                                <tr>
                                    <td><b>Gems</b></td>
                                    <td>{{ Auth::user()->bound_gems + Auth::user()->unbound_gems }}</td>
                                </tr>
                            </table>

                            <a href="/logout">Logout</a>
                            @if (Auth::user()->permissions == 1)
                               || <a href="/staff">Staff Panel</a>
                            @endif
                        </div>
                      @else
                        <h3>Login</h3>
                        <form role="form" method="POST" action="{{ url('/login') }}">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label">E-Mail Address</label>

                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label class="control-label">Password</label>

                              <input type="password" class="form-control" name="password">

                              @if ($errors->has('password'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                            </div>

                            <div class="form-group">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember"> Remember Me
                                  </label>
                              </div>
                            </div>

                            <div class="form-group">
                              <button type="submit" class="btn btn-custom">
                                  Login
                              </button>

                              <br><a class="forgot-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </form>
                      @endif
                </aside><br>

                <aside>
                  <h3>Chat Box</h3>
                  @include('cbox')
                </aside><br>
              </div>

            </div> <!-- #main -->
        </div> <!-- #main-container -->

        <div class="footer-container">
            <footer class="wrapper">
                  <a href="/mod_box.php">MOD BOX</a> |
                  <a href="/credits.php">CREDITS</a> |
                  <a href="/pp.php">PRIVACY POLICY</a> |
                  <a href="/tos.php">TERMS OF SERVICE</a> |
                  <a href="/staff.php">STAFF</a>

                  <div class="right">
                    <a href="/">Tremder.com</a> &copy; 2016, all rights reserved.
                  </div>
            </footer>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="/js/main.js"></script>
        @yield('scripts')

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
