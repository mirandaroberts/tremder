@extends('layouts.app')

@section('title')
    Edit Your Profile
@endsection

@section('header')
    Edit Your Profile
@endsection

@section('content')
    <div class="center">
        Use the form below to edit your profile.<br>
    </div>

    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="">
        {!! csrf_field() !!}

        <div class="center"><img src="images/avatars/{{ Auth::user()->id }}.png" width="200" height="200"></div><br>

        <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Avatar</label>

            <div class="col-md-6">
                <input type="file" class="form-control" name="avatar">

                @if ($errors->has('avatar'))
                    <span class="help-block">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">Bio</label>

            <div class="col-md-6">
                <textarea name="bio" class="form-control">{{ Auth::user()->bio }}</textarea>

                @if ($errors->has('bio'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bio') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Edit Profile
                </button>
            </div>
        </div>
    </form>
@endsection
