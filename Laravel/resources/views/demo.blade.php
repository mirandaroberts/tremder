@extends('layouts.app')

@section('title', 'Wolf Demo')
@section('header', 'Wolf Demo')

@section('content')
  <form class="form-horizontal" role="form" method="POST">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <div class="center" id="image" width="718" height="500">
    <img src='data:image/png;base64,{{  base64_encode($output) }}'>
  </div>

  <br><br>
  <h1>Basic Info</h1>

  <!--<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Gender</label>

    <div class="col-md-6">
      <select class="form-control option" name="gender" id="gender">
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>

      @if ($errors->has('gender'))
        <span class="help-block">
          <strong>{{ $errors->first('gender') }}</strong>
        </span>
      @endif
    </div>
  </div>-->

  <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Age</label>

    <div class="col-md-6">
      <select class="form-control option" name="age" id="age">
        <option value="7">1 Week</option>
        <option value="122">4-5 Months</option>
        <option value="365">1 Year</option>
        <option value="730" selected>2 Years</option>
        <option value="2555">7 Years</option>
        <option value="2738">7.5 Years</option>
        <option value="2920">8 Years</option>
        <option value="3103">8.5 Years</option>
        <option value="3285">9 Years</option>
        <option value="3468">9.5 Years</option>
        <option value="3650">10 Years</option>
      </select>

      @if ($errors->has('age'))
        <span class="help-block">
          <strong>{{ $errors->first('age') }}</strong>
        </span>
      @endif
    </div>
  </div>

  <br><h1>Base Colors</h1>

  <div class="form-group{{ $errors->has('eyeColor') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Eye Color</label>

    <div class="col-md-6">
      <select class="form-control option" name="eyeColor" id="eyeColor">
        @foreach ($species->eyeColors as $eye)
          <option value="{{ $eye->id }}">{{ $eye->name }}</option>
        @endforeach
      </select>

        @if ($errors->has('eyeColor'))
          <span class="help-block">
            <strong>{{ $errors->first('eyeColor') }}</strong>
          </span>
        @endif
    </div>
  </div>

  <div class="form-group{{ $errors->has('baseColor') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Base Color</label>

    <div class="col-md-6">
      <select class="form-control option" name="baseColor" id="baseColor">
        @foreach ($species->publicBaseColors as $base)
          <option value="{{ $base->id }}">{{ $base->name }}</option>
        @endforeach
      </select>

        @if ($errors->has('baseColor'))
          <span class="help-block">
            <strong>{{ $errors->first('baseColor') }}</strong>
          </span>
        @endif
    </div>
  </div>

  <br><h1>Markings</h1>
  <div class="center">Click "add a marking" to select markings for your wolf (limited to 20 markings).<br>Drag and drop markings to re-order (bottom most markings will show on top).<br><br></div>

  <div id="markings"></div>

  <div class="center">
    <button type="button" class="btn btn-primary marking">Add a Marking!</button>
  </div>
@endsection

@section('scripts')
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

  <script>
    $(document).ready(function() {
      // global variables
    	var page = {
    		check: false,
    		isLoading: false
    	};

      var mCount = 0;

      // Add marking fields on button click
      $('.marking').on('click', function() {
        if (mCount < 20) {
          $('#markings').append('<div class="form-group marking"><label class="col-md-4 control-label">Marking</label><div class="col-md-6"><select class="form-control markings" name="markings[]">' +
                                '<option disabled selected>Select a Marking</option>' +
                                 @foreach ($species->publicMarkings as $marking)
                                  '<option value="{{ $marking->id }}">{{ $marking->name }}</option>' +
                                 @endforeach
                                '</select></div>' +
                                '<div class="Colors"><label class="col-md-4 control-label">Color</label>' +
                                '<div class="col-md-6"><select class="form-control option markingColors" name="markingColors[]">' +
                                '<option disabled>Select a Color</option>' +
                                @foreach ($species->colors as $color)
                                 '<option value="{{ $color->id }}">{{ $color->name }}</option>' +
                                @endforeach
                                '</select></div></div>' +
                                '<label class="col-md-4 control-label">Opacity</label>' +
                                '<div class="col-md-6"><input type="range" class="opacity option" name="markingOpacity[]" value="1" step="0.01" min="0" max="1"></div>' +
                                '<br><span class="remove">Remove</span>' +
                                '</div><br>');
          } else {
            alert('You cannot add any more markings!');
          }

        mCount = mCount + 1;
      });

      // Sort Markings
      $('#markings').sortable({
        update: function(event, ui) {
          process();
        }
      });

      // Remove marking
      $(document).on('click', '.remove', function() {
        $(this).closest('.marking').remove();
        process();
      });

      // on markings change...
      $(document).on('change', '.markings', function() {
        var data = {
          _token: $('meta[name="csrf-token"]').attr('content'),
          Marking: $(this).val()
        };

        var t = $(this).closest('.marking');

        // Get marking info
        $.ajax({
          type: "POST",
          url: '/markingid',
          data: data,
          dataType: 'json',
          context: this,
          success: function(data) {
            if (data.type === 'error') {
              alert(data.message);
            } else {
              // If special marking, don't show color options.
              if (data.special == 1) {
                t.find('.Colors').hide();

                process();
              } else {
                t.find('.Colors').show();

                process();
              }

              // If marking has specific colors, show only those.

            }
          },
          error: function(data) {
            // Error...
            var errors = $.parseJSON(data.responseText);
            console.log(errors);
          }
        });
      });

      // On input update, process pet data
    	$(document).on('change', '.option', function() {
        process();
      });

      function process() {
    		if(!page.check){
    			page.check = true;

    			setTimeout(function(){
    				page.check = false;
    			}, 500);

          // Set base values
    			var PetData = {
    				_token: $('meta[name="csrf-token"]').attr('content'),
    				gender: $('#gender').val(),
            age: $('#age').val(),
            eyeColor: $('#eyeColor').val(),
            baseColor: $('#baseColor').val(),
            markings: $('.markings').serializeArray(),
            markingColors: $('.markingColors').serializeArray(),
            markingOpacity: $('.opacity').serializeArray(),
            species: "{{ $species->name }}"
    			};

          // Start loading screen
    			$('#image').prepend('<div style="position: absolute; background-color:rgba(238, 238, 238, 0.8); width: 720px; height: 500px;"><i class="fa fa-spinner fa-pulse fa-5x" style="position: absolute; left: 50%; top: 50%;"></i></div>');

    			// Stringify pet object
    			JSON.stringify(PetData);

    			// Ajax call
    			$.ajax({
  	        type: "POST",
  	        url: '/processpet',
  					data: PetData,
  					dataType: 'json',
  	        success: function(data) {
  						if (data.type === 'error') {
  							alert(data.message);
  						} else {
  	            $("#image").html("<img src='data:image/png;base64," + data + "'>");
  						}
  	        },
  					error: function(data) {
  						// Error...
  				    var errors = $.parseJSON(data.responseText);
  				    console.log(errors);
  		      }
    	    });
        }
      }
    });
  </script>
@endsection
