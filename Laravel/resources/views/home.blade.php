@extends('layouts.app')

@section('title', 'Home')
@section('header', 'Home')

@section('content')
  You are logged in!

  @if (Auth::user()->confirmed)
    <br><br>Your account has been verified!
  @else
    <br><br>Your account has not been verified yet!
  @endif
@stop
