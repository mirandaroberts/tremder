@extends('layouts.app')

@section('title')
    {{ $user->name }}'s Profile
@endsection

@section('header')
    {{ $user->name }}'s Profile
@endsection

@section('content')
    <div class="center">
        <img src="/images/avatars/{{ $user->id }}.png" width="200" height="200" class="icon"><br>
        <b>{{ $user->name }} (#{{ $user->id }})</b>

        @if ($user->last_active >= $now)
            <i class="fa fa-circle online" aria-hidden="true"></i>
        @else
            <i class="fa fa-circle-o" aria-hidden="true"></i>
        @endif
        <br>
        <span class="small-txt">Last Online {{ $user->last_active->diffForHumans() }}<br>Joined {{ $user->created_at->format('M d Y')}}</span><br>
        <a href="">Visit {{ $user->name }}'s Shop!</a>
    </div><br><br>

    <h1>Primary Wolves</h1>
    @foreach ($user->wolves as $wolf)
        @if ($wolf->primary)
            <div class="primary-wolves">
                <div class="left half">
                    <img src="/images/wolves/{{ $wolf->id }}.png">
                </div>
                <div class="right half">
                    <br><h1>{{ $wolf->name }}</h1>

                    <table width="50%">
                        <tr>
                            <td><b>Gender</b></td>
                            <td>{{ ucfirst($wolf->gender) }}</td>
                        </tr>
                        <tr>
                            <td><b>Age</b></td>
                            <td>{{ floor($wolf->age / 7) }} Weeks</td>
                        </tr>
                        <tr>
                            <td><b>Health</b></td>
                            <td>{{ $wolf->health }}</td>
                        </tr>
                    </table>
                </div>
                <br class="clear">
            </div>
        @endif
    @endforeach
    <br><br>

    <h1>Pack Wolves</h1>
    The rest of the wolves
    <br><br>

    <h1>About {{ $user->name }}</h1>
    {!! $user->bio !!}
@endsection
