@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Manage Pet Species')

@section('content')
Pets marked "not public" are not yet creatable through the on-site creation system. Be sure to update the files before a new marking is made public, and be sure ALL files are in place for both genders and baby before setting a pet to "public." Otherwise, broken images will occur.<br><br>
  <ol>
    @foreach ($species as $s)
      <li>
        <b>{{ ucfirst($s->name) }}</b>

        @if ($s->public == 0)
          (not public)
        @endif

         <a href="/staff/species/edit/{{ $s->name }}">Manage Files</a>
      </li>
    @endforeach
  </ol>
@stop
