@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Create New eye Color')

@section('content')
  Enter the name of the eye color you would like to create below. You will have to create the colors seperately for each species, to allow for each species to have it's own unique set of eye colors.<br><br>

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/staff/eyecolors/create') }}">
    {!! csrf_field() !!}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label class="col-md-4 control-label">Name</label>

      <div class="col-md-6">
        <input type="text" class="form-control" name="name" value="{{ old('name') }}">

          @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('data_species_id') ? ' has-error' : '' }}">
      <label class="col-md-4 control-label">Species</label>

      <div class="col-md-6">
        <select class="form-control" name="data_species_id" value="{{ old('data_species_id') }}">
          @foreach($species as $s)
            <option value="{{ $s->id }}">{{ $s->name }}</option>
          @endforeach
        </select>

          @if ($errors->has('data_species_id'))
            <span class="help-block">
              <strong>{{ $errors->first('data_species_id') }}</strong>
            </span>
          @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
          Create Eye Color
        </button>
      </div>
    </div>
  </form>
@stop
