@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Staff Panel')

@section('content')
  Select a base to get started.<br>

  @foreach ($species as $s)
    <a href="/staff/demo/{{ $s->name }}"><h2>{{ ucfirst($s->name) }}</h2></a>
  @endforeach
@stop
