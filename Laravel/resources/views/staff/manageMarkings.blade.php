@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Manage Markings')

@section('content')
Markings marked "not public" are not yet creatable through the on-site creation system. Be sure to update the files before a new marking is made public, and be sure ALL files are in place for both genders and baby before setting a marking to "public." Otherwise, broken images will occur.<br><br>

  @foreach ($species as $s)
  <h1>{{ $s->name }}</h1>

    <ol>
      @foreach ($s->markings as $m)
        <li>
          <b>{{ $m->name }}</b>

          @if ($m->public == 0)
            (not public)
          @endif

          @if ($m->special != NULL)
            (special)
          @endif

           <a href="/staff/marking/edit/{{ $m->id }}">Manage Marking</a>
        </li>
      @endforeach
    </ol>
  @endforeach
@stop
