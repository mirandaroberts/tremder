@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Manage Beta Codes')

@section('content')
  You may delete and resend beta codes that have not been used. Once a beta code has been used, it may not be removed for tracking and security purposes. If the user needs a different email, delete the old beta code and create a new beta code using the <a href="/staff/betacodes/create">Generate Beta Code tool</a>.<br><br>
  <ol>
    @foreach ($codes as $code)
      <li>
        <b>{{ $code->email }}</b> -
        @if ($code->code == '')
          <i>Used</i>
        @else
          {{ $code->code }} <a href="/staff/betacodes/delete/{{ $code->id }}">Delete</a> || <a href="/staff/betacodes/resend/{{ $code->id }}">Resend</a>
        @endif
      </li>
    @endforeach
  </ol>
@stop
