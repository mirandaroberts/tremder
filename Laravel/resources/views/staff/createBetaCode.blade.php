@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Beta Code Generation')

@section('content')
  Enter the email that will recieve the beta code. The recipient will have to use the email that is associated with the code, so be sure to send it to the email they wish to sign up with!<br><br>

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/staff/betacodes/create') }}">
      {!! csrf_field() !!}

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">User Email</label>

          <div class="col-md-6">
              <input type="email" class="form-control" name="email" value="{{ old('email') }}">

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="btn btn-primary">
                  Send Code
              </button>
          </div>
      </div>
  </form>
@stop
