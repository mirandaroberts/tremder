@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Create New Species')

@section('content')
  Enter the name of the species you would like to create below. This tool will create all the necessary directories and prep the site for its launch. You will be redirected to the species manager, where you can upload the marking, mutations, and other files associated with the breed.<br><br>

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/staff/species/create') }}">
    {!! csrf_field() !!}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label class="col-md-4 control-label">Species Name</label>

      <div class="col-md-6">
        <input type="text" class="form-control" name="name" value="{{ old('name') }}">

          @if ($errors->has('name'))
            <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
          Create Species
        </button>
      </div>
    </div>
  </form>
@stop
