@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Edit Species')

@section('content')
  Use the fields below to edit the {{ $species->name }}. Any files uploaded will overwrite any existing files, so be careful to upload in the correct fields. The uploads may take some time to process if you are uploading a lot, so be patient to let the page redirect on it's own.<br><br>

  <div id="missing">
    The following files are missing. This pet cannot be made public until all files have been uploaded.<br>

    <table>
      <tr>
          @foreach ($missing as $g => $m)
          <td>
            <b>Missing {{ $g }} files</b>
            <ul>
              @foreach ($m as $v)
                <li>{{ $v }}</li>
              @endforeach
            </ul>
          </td>
          @endforeach
      </tr>
    </table>
  </div>
  <br><br>

  <div>
    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
      {!! csrf_field() !!}

      <h1>Base Files</h1>

      <div class="form-group{{ $errors->has('m_lineart') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Male Lineart</label>

        <div class="col-md-6">
          <input type="file" class="form-control" name="items[m_lineart]" value="m_lineart">

            @if ($errors->has('m_lineart'))
              <span class="help-block">
                <strong>{{ $errors->first('m_lineart') }}</strong>
              </span>
            @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('f_lineart') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Female Lineart</label>

        <div class="col-md-6">
          <input type="file" class="form-control" name="items[f_lineart]" value="f_lineart">

            @if ($errors->has('f_lineart'))
              <span class="help-block">
                <strong>{{ $errors->first('f_lineart') }}</strong>
              </span>
            @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('b_lineart') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Baby Lineart</label>

        <div class="col-md-6">
          <input type="file" class="form-control" name="items[b_lineart]" value="b_lineart">

            @if ($errors->has('b_lineart'))
              <span class="help-block">
                <strong>{{ $errors->first('b_lineart') }}</strong>
              </span>
            @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('m_base_nose') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Male Base Nose</label>

        <div class="col-md-6">
          <input type="file" class="form-control" name="items[m_base_nose]" value="m_base_nose">

            @if ($errors->has('m_base_nose'))
              <span class="help-block">
                <strong>{{ $errors->first('m_base_nose') }}</strong>
              </span>
            @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('f_base_nose') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Female Base Nose</label>

        <div class="col-md-6">
          <input type="file" class="form-control" name="items[f_base_nose]" value="f_base_nose">

            @if ($errors->has('f_base_nose'))
              <span class="help-block">
                <strong>{{ $errors->first('f_base_nose') }}</strong>
              </span>
            @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('b_base_color') ? ' has-error' : '' }}">
        <label class="col-md-4 control-label">Baby Default Color</label>

        <div class="col-md-6">
          <input type="file" class="form-control" name="items[b_base_color]" value="b_base_color">

            @if ($errors->has('b_base_color'))
              <span class="help-block">
                <strong>{{ $errors->first('b_base_color') }}</strong>
              </span>
            @endif
        </div>
      </div>
      <br><br>

      <h1>Eye Colors</h1>
      The naming in this section is extremely important! Please name your files the name of the trait all lower case with underscores in place of spaces. For instance Light Green would be light_green.png.

      <div class="left half">
        <div class="center"><h2>Male</h2></div>
        @foreach ($species->eyeColors as $m)
          <div class="form-group{{ $errors->has('eye.m.' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="eye.m.{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('m' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>

      <div class="right half">
        <div class="center"><h2>Female</h2></div>
        @foreach ($species->eyeColors as $m)
          <div class="form-group{{ $errors->has('eye.f.' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="eye.f.{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('f' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>
      <br class="clear"><br>

      <h1>Base Colors</h1>

      <div class="left half">
        <div class="center"><h2>Male</h2></div>
        @foreach ($species->baseColors as $m)
          <div class="form-group{{ $errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="m_{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('m' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>

      <div class="right half">
        <div class="center"><h2>Female</h2></div>
        @foreach ($species->baseColors as $m)
          <div class="form-group{{ $errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="f_{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('f' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>
      <br class="clear"><br>

      <h1>Mutations</h1>

      <div class="left half">
        <div class="center"><h2>Male</h2></div>
        @foreach ($species->mutations as $m)
          <div class="form-group{{ $errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="m_{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('m' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>

      <div class="right half">
        <div class="center"><h2>Female</h2></div>
        @foreach ($species->mutations as $m)
          <div class="form-group{{ $errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="f_{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('f' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>
      <br class="clear">

      <div class="center"><h2>Baby</h2></div>
      @foreach ($species->mutations as $m)
        <div class="form-group{{ $errors->has('b_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">{{ $m->name }}</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="b_{{strtolower(str_replace(" ", "_", $m->name))}}">

              @if ($errors->has('b_' . strtolower(str_replace(" ", "_", $m->name))))
                <span class="help-block">
                  <strong>{{ $errors->first('b' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                </span>
              @endif
          </div>
        </div>
      @endforeach

      <br><br>

      <h1>Aging Effects</h1>

      <div class="right half">
        <div class="center"><h2>Female</h2></div>
        <div class="form-group{{ $errors->has('f_age_7') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">7 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_7">

              @if ($errors->has('f_age_7'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_7') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('f_age_7.5') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">7.5 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_7.5">

              @if ($errors->has('f_age_7.5'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_7.5') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('f_age_8') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">8 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_8">

              @if ($errors->has('f_age_8'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_8') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('f_age_8.5') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">8.5 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_8.5">

              @if ($errors->has('f_age_8.5'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_8.5') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('f_age_9') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">9 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_9">

              @if ($errors->has('f_age_9'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_9') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('f_age_9.5') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">9.5 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_9.5">

              @if ($errors->has('f_age_9.5'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_9.5') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('f_age_10') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">10 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="f_age_10">

              @if ($errors->has('f_age_10'))
                <span class="help-block">
                  <strong>{{ $errors->first('f_age_10') }}</strong>
                </span>
              @endif
          </div>
        </div>
      </div>

      <div class="left half">
        <div class="center"><h2>Male</h2></div>
        <div class="form-group{{ $errors->has('m_age_7') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">7 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_7">

              @if ($errors->has('m_age_7'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_7') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('m_age_7.5') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">7.5 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_7.5">

              @if ($errors->has('m_age_7.5'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_7.5') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('m_age_8') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">8 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_8">

              @if ($errors->has('m_age_8'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_8') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('m_age_8.5') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">8.5 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_8.5">

              @if ($errors->has('m_age_8.5'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_8.5') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('m_age_9') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">9 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_9">

              @if ($errors->has('m_age_9'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_9') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('m_age_9.5') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">9.5 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_9.5">

              @if ($errors->has('m_age_9.5'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_9.5') }}</strong>
                </span>
              @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('m_age_10') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label">10 years</label>

          <div class="col-md-6">
            <input type="file" class="form-control" name="m_age_10">

              @if ($errors->has('m_age_10'))
                <span class="help-block">
                  <strong>{{ $errors->first('m_age_10') }}</strong>
                </span>
              @endif
          </div>
        </div>
      </div>
      <br class="clear"><br>

      <h1>Markings</h1>

      <div class="right half">
        <div class="center"><h2>Male</h2></div>
        @foreach ($species->markings as $m)
          <div class="form-group{{ $errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="m_{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('m_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('m' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>

      <div class="left half">
        <div class="center"><h2>Female</h2></div>
        @foreach ($species->markings as $m)
          <div class="form-group{{ $errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))) ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">{{ $m->name }}</label>

            <div class="col-md-6">
              <input type="file" class="form-control" name="f_{{strtolower(str_replace(" ", "_", $m->name))}}">

                @if ($errors->has('f_' . strtolower(str_replace(" ", "_", $m->name))))
                  <span class="help-block">
                    <strong>{{ $errors->first('f' . strtolower(str_replace(" ", "_", $m->name))) }}</strong>
                  </span>
                @endif
            </div>
          </div>
        @endforeach
      </div>
      <br class="clear"><br>

      <div class="form-group center">
        <button type="submit" class="form-control btn btn-primary ninety">
          Edit {{ $species->name }}
        </button>
      </div>
    </form>
  </div>
@stop
