@extends('layouts.app')

@section('title', 'Staff Panel')
@section('header', 'Staff Panel')

@section('content')
  Here is a staff panel! We will have all sorts of fun stuff to do here! :D

  <br><br><h2>User Functions</h2>
  <a href="/staff/betacodes/create">Generate Beta Code</a><br>
  <a href="/staff/betacodes">Manage Beta Codes</a><br>
  <a href="/staff/manageusers">Manage User Permissions</a><br>
  <a href="/staff/freezeuser">Freeze a User</a>

  <br><h2>Pet Functions</h2>
  <a href="/staff/species/create">Create New Species</a><br>
  <a href="/staff/markings/create">Create New Marking</a><br>
  <a href="/staff/mutations/create">Create New Mutation</a><br>
  <a href="/staff/eyecolors/create">Create New Eye Color</a><br>
  <a href="/staff/basecolors/create">Create New Base Color</a><br>
  <a href="/staff/colors/create">Create New Palette Color</a><br><br>
  <a href="/staff/species">Manage Species</a><br>
  <a href="/staff/markings">Manage Markings</a><br>
  <a href="/staff/mutations">Manage Mutations</a><br>
  <a href="/staff/eyecolors">Manage Eye Colors</a><br>
  <a href="/staff/basecolors">Manage Base Colors</a><br>
  <a href="/staff/palette">Manage Palette Colors</a><br><br>
  <a href="/staff/demo">Staff Demo</a><br>
@stop
