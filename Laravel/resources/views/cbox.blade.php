<div class="cbox">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <div class="center">Be sure to read our <a href="">Chat Rules</a>!</div>
  <div class="chat-posts">
      <div class="center">
        @if (Auth::user())
          You are chatting as <b>{{ Auth::user()->name }}</b>!
        @else
          You are chatting as <b>Guest</b>!
        @endif
      </div><hr>
      <div class="chat-log"></div>
      <div id="sendie"><input type="text" name="chat_body" class="chat-body"> <i class="fa fa-paper-plane"></i></div>
  </div><br>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    // Scrolls to bottom immediately
    $(window).load(function() {
      $('.chat-posts').find('.chat-log').animate({ scrollTop: $(document).height() }, 1000);
    })

    // Chat updates every 500ms
    window.setInterval(function() {
      update();
    }, 500);

    // Post a new message
    $('#sendie').keypress(
    function(e){
        if (e.keyCode == 13) {
          event.stopPropagation();
          event.preventDefault();

          newMsg();
          update();

          $('.chat-posts').find('.chat-log').animate({ scrollTop: $(document).height() }, 1000);
        }
    });

    $('.fa-paper-plane').on('click', function() {
      event.stopPropagation();
      event.preventDefault();

      newMsg();
      update();

      $('.chat-posts').find('.chat-log').animate({ scrollTop: $(document).height() }, 1000);
    });

    function newMsg() {
      var data = {
        _token: $('meta[name="csrf-token"]').attr('content'),
        chat_body: $('.chat-body').val(),
      }

      $.ajax({
	        type: "POST",
          dataType: 'JSON',
          data: data,
	        url: '/postchat',
	        success: function(data) {
						if (data.type === 'error') {
							alert(data.message);
            } else {
              $('.chat-body').val('');
              $('.chat-posts').find('.chat-log').animate({ scrollTop: $(document).height() }, 1000);
            }
	        },
					error: function(data) {
						// Error...
				    var errors = $.parseJSON(data.responseText);
				    console.log(errors);
		      }
	    });
    }

    function update() {
      var data = {
        _token: $('meta[name="csrf-token"]').attr('content')
      }

			$.ajax({
	        type: "POST",
          dataType: 'JSON',
          data: data,
	        url: '/updatechat',
	        success: function(data) {
						if (data.type === 'error') {
							alert(data.message);
						} else {
              // Update chat rooms
              $('.chat-posts').find('.chat-log').html('');
              $.each(data.messages, function(i, obj) {
									{
                    $('.chat-posts').find('.chat-log').append('<b>' + obj.author + '</b>: ' + obj.body + '<div class="chat-ts">' + obj.ts + '</div>');
									}
							});
            }
	        },
					error: function(data) {
						// Error...
				    var errors = $.parseJSON(data.responseText);
				    console.log(errors);
		      }
	    });
    }
  });
</script>
