@extends('layouts.app')

@section('title')
    Your Territory
@endsection

@section('header')
    Your Territory
@endsection

@section('content')
    <div class="center">
        @if (Auth::user()->wolves->count() == 0)
            It seems this is the first time visiting your territory! To get started, you will be granted a pair of wolves to start off your wolf pack.<br><br>

            <a href="/firstwolves"><button class="btn btn-primary">Give me my wolves!</button></a>
        @else
            <h1>Primary Wolves</h1>
            @foreach (Auth::user()->wolves as $wolf)
                @if ($wolf->primary)
                    <div class="primary-wolves">
                        <div class="left half">
                            <img src="/images/wolves/{{ $wolf->id }}.png">
                        </div>
                        <div class="right half">
                            <br><h1>{{ $wolf->name }}</h1>

                            <table width="50%">
                                <tr>
                                    <td><b>Gender</b></td>
                                    <td>{{ ucfirst($wolf->gender) }}</td>
                                </tr>
                                <tr>
                                    <td><b>Age</b></td>
                                    <td>{{ floor($wolf->age / 7) }} Weeks</td>
                                </tr>
                                <tr>
                                    <td><b>Health</b></td>
                                    <td>{{ $wolf->health }}</td>
                                </tr>
                            </table>

                            <br><button class="btn btn-primary">Edit Wolf</button>
                        </div>
                        <br class="clear">
                    </div>
                @endif
            @endforeach
            <br><br>

            <h1>Pack Wolves</h1>
            The rest of the wolves
        @endif
    </div>
@endsection
